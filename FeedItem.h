//
//  FeedItem.h
//  RssReader
//
//  Created by Ihar Zharski on 5/2/13.
//  Copyright (c) 2013 Ihar Zharski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FeedItem : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSDate * pubDate;

@end
