//
//  FeedParser.h
//  RssReader
//
//  Created by Ihar Zharski on 5/2/13.
//  Copyright (c) 2013 Ihar Zharski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedParser : NSObject <NSXMLParserDelegate>
- (void)parseFeed:(NSURL *)url intoContext:(id)context;
@end
