//
//  RSSReaderTVC.h
//  RssReader
//
//  Created by Ihar Zharski on 5/2/13.
//  Copyright (c) 2013 Ihar Zharski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface RSSReaderTVC : UITableViewController <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSMutableArray *feedItems;
@end
