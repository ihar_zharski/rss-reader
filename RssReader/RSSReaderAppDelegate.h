//
//  RSSReaderAppDelegate.h
//  RssReader
//
//  Created by Ihar Zharski on 5/2/13.
//  Copyright (c) 2013 Ihar Zharski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSSReaderAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
