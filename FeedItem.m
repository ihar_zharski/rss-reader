//
//  FeedItem.m
//  RssReader
//
//  Created by Ihar Zharski on 5/2/13.
//  Copyright (c) 2013 Ihar Zharski. All rights reserved.
//

#import "FeedItem.h"


@implementation FeedItem

@dynamic title;
@dynamic itemDescription;
@dynamic link;
@dynamic pubDate;

@end
